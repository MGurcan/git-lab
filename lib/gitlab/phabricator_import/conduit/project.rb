# frozen_string_literal: true
module Gitlab
    module PhabricatorImport
      module Conduit
        class Project
          MAX_PAGE_SIZE = 100

          def initialize(phabricator_url:, api_token:)
            @client = Client.new(phabricator_url, api_token)
          end

          def projects(phids)
            phids.each_slice(MAX_PAGE_SIZE).map { |limited_phids| get_page(limited_phids) }
          end

          private

          def get_page(phids)
            ProjectsResponse.new(get_projects(phids))
          end

          def get_projects(phids)
            client.get('project.search',
                       params: { constraints: { phids: phids } })
          end

          attr_reader :client
        end
      end
    end
  end

