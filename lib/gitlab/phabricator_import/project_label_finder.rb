# frozen_string_literal: true

module Gitlab
    module PhabricatorImport
        class ProjectLabelFinder
            def initialize(project, phids)
              @project, @phids = project, phids
              @loaded_phids = Set.new
            end

            def find_or_create(phid)
              # Wrapping this in an `object_map.get_gitlab_model` is so we don't need
              # to request or find the label multiple times for multiple parallel
              # jobs that are importing Phabricator-tasks that might use the same phid
              label = object_map.get_gitlab_model(phid) do
                find_or_create_label_for_phid(phid)
              end

              # adding the phid to the loaded phids prevents us from
              # having to request it when the phid wasn't loaded through the API
              # but through a cache git from `object_map.get_gitlab_model`
              @loaded_phids << phid

              label
            end

            private

            def object_map
              @object_map ||= Gitlab::PhabricatorImport::Cache::Map.new(project)
            end

            def find_or_create_label_for_phid(phid)
              phabricator_project = phabricator_projects.find { |p| p.phabricator_id == phid }
              # If the project was no longer found in Phabricator
              return unless phabricator_project

              # create a new label for the phabricator project using the existing service for that
              # https://gitlab.com/gitlab-org/gitlab/blob/master/app/services/labels/find_or_create_service.rb#L4
              # we can skip authorization for an import.
              Labels::FindOrCreateService.new(nil, project, title: phabricator_project.name).execute(skip_authorization: true)
            end

            def project_phids_to_request
              phids - loaded_phids.to_a
            end

            def phabricator_projects
              @phabricator_projects ||= client.projects(project_phids_to_request)
            end

            def client
              # We'll need to create this class to perform the requests to the Phabricator API
              @client ||= Gitlab::PhabricatorImport::Conduit::Project
                            .new(phabricator_url: project.import_data.data['phabricator_url'],
                                 api_token: project.import_data.credentials[:api_token])
            end
          end

    end
end
